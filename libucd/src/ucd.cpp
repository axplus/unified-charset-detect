// ucd.cpp
// Created by yjzeng, Sep 25, 2010


// DO NOT USE PRECOMPILED-HEADER - #include "eedpch.h"


#include <string>

#include <nscore.h>
#include <nsUniversalDetector.h>

#include "ucd.h"


static bool detectbom(std::string* charsetname,
                      byte* block, size_t blocksize);
// static std::string slice(byte* block, size_t begin, size_t end);


class UCDWrapper : public nsUniversalDetector {
public:
    UCDWrapper() :
        nsUniversalDetector(NS_FILTER_ALL),
        charset_() {
    }

    const std::string& charset() const {
        return charset_;
    }

protected:

    virtual void Report(const char *_charset) {
        charset_ = _charset;
    }

private:

    std::string charset_;

};


class BomEntity {
public:
    BomEntity() :
        charsetname_(),
        bomsize(0) {
    }

    BomEntity(const std::string& charsetname,
              byte byte1, byte byte2) :
        charsetname_(charsetname),
        bomsize(2) {
        bytes_[0] = byte1;
        bytes_[1] = byte2;
    }

    BomEntity(const std::string& charsetname,
              byte byte1, byte byte2, byte byte3) :
        charsetname_(charsetname),
        bomsize(3) {
        bytes_[0] = byte1;
        bytes_[1] = byte2;
        bytes_[2] = byte3;
    }

    BomEntity(const std::string& charsetname,
              byte byte1, byte byte2, byte byte3, byte byte4) :
        charsetname_(charsetname),
        bomsize(4) {
        bytes_[0] = byte1;
        bytes_[1] = byte2;
        bytes_[2] = byte3;
        bytes_[3] = byte4;
    }

    const std::string& charsetname() const {
        return charsetname_;
    }

    bool match(byte* block, size_t blocksize) const {
        // bytes_[0..bomsize] == block[0..bomsize]
        return std::equal(bytes_, &bytes_[bomsize], block);
    }
private:
    std::string charsetname_;
    byte bytes_[4];
    size_t bomsize;
};


static const BomEntity registerBomEntities[] = {
    BomEntity("UTF-16LE", 0xff, 0xfe),
    BomEntity("UTF-16BE", 0xfe, 0xff),
    BomEntity("UTF-8",    0xef, 0xbb, 0xbf),
    BomEntity("UTF-32LE", 0xff, 0xfe, 0x00, 0x00),
    BomEntity("UTF-32BE", 0x00, 0x00, 0xfe, 0xff),
    BomEntity(),
};


bool detectcharset(std::string* charsetname,
                   bool* withsignature,
                   byte* byteblock, size_t blocksize) {
    if ((*withsignature = detectbom(charsetname,
                                    byteblock,
                                    blocksize))) {
        return true;
    }
    UCDWrapper wrpr;
    wrpr.HandleData((const char*)byteblock, blocksize);
    wrpr.DataEnd();
    *charsetname = wrpr.charset();
    return !charsetname->empty();
}


// ͨ��bom���block�ı���
static bool detectbom(std::string* charsetname,
                      byte* block, size_t blocksize) {
    for (const BomEntity* be = &registerBomEntities[0];
         !be->charsetname().empty();
         ++be) {
        if (be->match(block, blocksize)) {
            *charsetname = be->charsetname();
            return true;
        }
    }
    return false;
}
