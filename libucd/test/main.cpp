// main.c
// Created by yjzeng, Sep 21, 2010


#include <string>
#include <stdio.h>
#include "ucd.h"


void fillblock(const std::string& filename,
               size_t maxsize,
               byte* block, size_t* size) {
    FILE* fp = fopen(filename.c_str(), "rb");
    fseek(fp, 0, SEEK_END);
    fpos_t lastpos = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    fread(block, std::min(maxsize, (size_t)lastpos), 1, fp);
    *size = lastpos;
    fclose(fp);
}


void test_chardet(const std::string& filename,
                  const std::string& except) {
    byte block[256] = {0};
    size_t size;
    fillblock(filename,
              /*maxsize*/ 256,
              block, &size);
    std::string charsetname;
    bool withsignature;
    detectcharset(&charsetname, &withsignature, block, size);
    printf("-- except %s is %s", filename.c_str(), except.c_str());
    if (charsetname == except) {
        printf("...ok\n");
    } else {
        printf("...but got %s failed\n", charsetname.c_str());
    }
}


int main(int argc, char *argv[]) {
    if (argc < 3) {
        return 1;
    }
    test_chardet(argv[1], argv[2]);
    return 0;
}
