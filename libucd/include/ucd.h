// ucd.h
// Created by yjzeng, Sep 21, 2010

#ifndef UCD_H
#define UCD_H


typedef unsigned char byte;


bool detectcharset(std::string* charsetname,
                   bool* withsignatuure,
                   byte* block, size_t blocksize);

//c only
int detectcharset_c(byte *block, int block_size, const char *charset_name, int *with_sign);

#endif // UCD_H
