// detect.cpp
// Created by yjzeng, 2011-10-30


#include <stdio.h>
#include <string>
#include <algorithm>
#include "ucd.h"

static void readfile(const std::string& filename, size_t maxsize,
                     byte* block, size_t* actualsize);


int main(int argc, char** argv) {
    std::string charsetname;
    bool withsignature;
    size_t filesize;
    byte block[1024] = {0};
    if (argc < 2) {
        fprintf(stderr, "no input file\n"); ////----
        return 1;
    }

    readfile(argv[1], 1024, block, &filesize);

    bool result = detectcharset(&charsetname, &withsignature,
                                block, filesize);
    if (result) {
        printf("%s", charsetname.c_str());
        if (withsignature) {
            printf(" WITH-BOM");
        }
        printf("\n"); ////----
        return 0;
    }

    fprintf(stderr, "can not detected\n"); ////----
    return 1;
}


void readfile(const std::string& filename, size_t maxsize,
              byte* block, size_t* actualsize) {
    FILE* fp = fopen(filename.c_str(), "r");

    fseek(fp, 0, SEEK_END);
    fpos_t lastpos = ftell(fp);
    size_t readbytes = std::min((size_t)lastpos, maxsize);
    fseek(fp, 0, SEEK_SET);

    fread(block, 1, readbytes, fp);
    *actualsize = readbytes;

    fclose(fp);
}
