srcdir=$(CURDIR)
timestamp=$(shell date +'%F %T')


.PHONY: all debug32 release32 push


all:


debug32: export CPPFLAGS+=-DDEBUG -D_DEBUG
release32: export CPPFLAGS+=-DNDEBUG
debug32 release32: export CXXFLAGS+=-arch i386
debug32 release32: export CFLAGS+=-arch i386
debug32 release32: export LDFLAGS+=-arch i386


debug32 release32: ./detect/detect


clean:
	$(RM) -rd debug32 release32


push:
	-git commit -a -m '$(timestamp)'
	git push


./detect/detect::
	mkdir -p $(MAKECMDGOALS)/$(@D) && $(MAKE) -C $(MAKECMDGOALS)/$(@D) -f $(srcdir)/detect/Makefile all SRCDIR=$(srcdir)/detect
